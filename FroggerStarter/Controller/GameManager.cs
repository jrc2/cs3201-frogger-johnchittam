﻿using System;
using System.Drawing;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using FroggerStarter.Model;
using FroggerStarter.View.Sprites;

namespace FroggerStarter.Controller
{
    /// <summary>
    ///     Manages all aspects of the game play including moving the player,
    ///     the Vehicles as well as lives and score.
    /// </summary>
    public class GameManager
    {
        #region Data members

        private const int BottomLaneOffset = 5;
        private const int PlayerMinX = 0;
        private const int PlayerMaxX = 600;
        private const int PlayerMinY = 55;
        private const int PlayerMaxY = 355;
        private const double SpeedToAddOnRespawn = 0.02;
        private readonly double backgroundHeight;
        private readonly double backgroundWidth;
        private readonly RoadManager roadManager;
        private int lives = 3;
        private int score;
        private Canvas gameCanvas;
        private Player player;
        private DispatcherTimer timer;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        /// </summary>
        /// <param name="backgroundHeight">Height of the background.</param>
        /// <param name="backgroundWidth">Width of the background.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     backgroundHeight &lt;= 0
        ///     or
        ///     backgroundWidth &lt;= 0
        /// </exception>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.backgroundHeight = backgroundHeight;
            this.backgroundWidth = backgroundWidth;
            this.roadManager = new RoadManager();

            this.setupGameTimer();
        }

        #endregion

        #region Methods

        public event EventHandler<PlayerEventArgs> PlayerLivesOrScoreUpdated;

        private void setupGameTimer()
        {
            this.timer = new DispatcherTimer();
            this.timer.Tick += this.timerOnTick;
            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 15);
            this.timer.Start();
        }

        /// <summary>
        ///     Initializes the game working with appropriate classes to play frog
        ///     and vehicle on game screen.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="gamePage">The game page.</param>
        /// <exception cref="ArgumentNullException">gameCanvas</exception>
        public void InitializeGame(Canvas gamePage, Canvas road)
        {
            this.gameCanvas = gamePage ?? throw new ArgumentNullException(nameof(gamePage));
            this.createAndPlacePlayer();
            this.gameCanvas.Children.Add(this.roadManager.CreateRoad(this.gameCanvas.Height, this.gameCanvas.Width)); 
        }

        private void createAndPlacePlayer()
        {
            this.player = new Player();
            this.gameCanvas.Children.Add(this.player.Sprite);
            this.setPlayerToCenterOfBottomLane();
        }

        private void setPlayerToCenterOfBottomLane()
        {
            this.player.X = this.backgroundWidth / 2 - this.player.Width / 2;
            this.player.Y = this.backgroundHeight - this.player.Height - BottomLaneOffset;
        }

        private void timerOnTick(object sender, object e)
        {
            foreach (var lane in this.roadManager.Lanes)
            {
                foreach (var vehicle in lane.Vehicles)
                {
                    this.moveVehicle(lane, vehicle);

                    this.checkForCollision(vehicle);
                }
            }
        }

        private void checkForCollision(Vehicle vehicle)
        {
            var vehicleRect = this.createRectangleForSprite(vehicle.Sprite);
            var playerSprite = this.createRectangleForSprite(this.player.Sprite);

            if (vehicleRect.IntersectsWith(playerSprite))
            {
                this.setPlayerToCenterOfBottomLane();
                this.lives--;
                this.onPlayerLivesOrScoreUpdated();
                this.resetLaneSpeeds();
            }
        }

        private RectangleF createRectangleForSprite(BaseSprite sprite)
        {
            var spriteRectangle = new RectangleF {
                X = (float) Canvas.GetLeft(sprite),
                Y = (float) Canvas.GetTop(sprite),
                Width = (float) sprite.Width,
                Height = (float) sprite.Height
            };

            return spriteRectangle;
        }

        private void resetLaneSpeeds()
        {
            foreach (var lane in this.roadManager.Lanes)
            {
                lane.Speed = lane.OriginalSpeed;
            }
        }

        private void moveVehicle(LaneManager lane, Vehicle vehicle)
        {
            if (lane.Direction == VehicleDirections.Right)
            {
                if (Canvas.GetLeft(vehicle.Sprite) >= this.gameCanvas.Width + vehicle.Sprite.Width)
                {
                    Canvas.SetLeft(vehicle.Sprite, 0);
                    lane.Speed += SpeedToAddOnRespawn;
                }
                else
                {
                    Canvas.SetLeft(vehicle.Sprite, Canvas.GetLeft(vehicle.Sprite) + lane.Speed);
                }
            }
            else
            {
                if (Canvas.GetLeft(vehicle.Sprite) <= 0)
                {
                    Canvas.SetLeft(vehicle.Sprite, this.gameCanvas.Width);
                    lane.Speed += SpeedToAddOnRespawn;
                }
                else
                {
                    Canvas.SetLeft(vehicle.Sprite, Canvas.GetLeft(vehicle.Sprite) - lane.Speed);
                }
            }
        }

        private void onPlayerLivesOrScoreUpdated()
        {
            var data = new PlayerEventArgs {Score = this.score, Lives = this.lives};
            this.PlayerLivesOrScoreUpdated?.Invoke(this, data);
        }

        /// <summary>
        ///     Moves the player to the left.
        ///     Precondition: none
        ///     Postcondition: player.X = player.X@prev - player.Width
        /// </summary>
        public void MovePlayerLeft()
        {
            if (this.player.X > PlayerMinX)
            {
                this.player.MoveLeft();
            }
        }

        /// <summary>
        ///     Moves the player to the right.
        ///     Precondition: none
        ///     Postcondition: player.X = player.X@prev + player.Width
        /// </summary>
        public void MovePlayerRight()
        {
            if (this.player.X < PlayerMaxX)
            {
                this.player.MoveRight();
            }
        }

        /// <summary>
        ///     Moves the player up.
        ///     Precondition: none
        ///     Postcondition: player.Y = player.Y@prev - player.Height
        /// </summary>
        public void MovePlayerUp()
        {
            if (this.player.Y > PlayerMinY)
            {
                this.player.MoveUp();
            }

            if ((int) this.player.Y == PlayerMinY)
            {
                this.score++;
                this.onPlayerLivesOrScoreUpdated();
                this.setPlayerToCenterOfBottomLane();
            }
        }

        /// <summary>
        ///     Moves the player down.
        ///     Precondition: none
        ///     Postcondition: player.Y = player.Y@prev + player.Height
        /// </summary>
        public void MovePlayerDown()
        {
            if (this.player.Y < PlayerMaxY)
            {
                this.player.MoveDown();
            }
        }

        #endregion
    }

    public class PlayerEventArgs : EventArgs
    {
        #region Properties

        public int Lives { get; set; }
        public int Score { get; set; }

        #endregion
    }
}