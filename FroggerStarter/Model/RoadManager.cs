﻿using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace FroggerStarter.Model
{
    /// <summary>
    ///     Manages all Lanes on the road
    /// </summary>
    public class RoadManager
    {
        #region Properties

        /// <summary>
        ///     The Lanes
        /// </summary>
        public IList<LaneManager> Lanes { get; }

        #endregion

        private const int LaneOneLocation = 305;
        private const int LaneWidth = 50;
        private readonly RotateTransform vehicleRotateTransform;

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoadManager" /> class.
        /// </summary>
        public RoadManager()
        {
            this.Lanes = new List<LaneManager> {
                new LaneManager(2, VehicleTypes.Car, 3, VehicleDirections.Left),
                new LaneManager(3, VehicleTypes.Semi, 4, VehicleDirections.Right),
                new LaneManager(3, VehicleTypes.Car, 5, VehicleDirections.Left),
                new LaneManager(2, VehicleTypes.Semi, 6, VehicleDirections.Left),
                new LaneManager(3, VehicleTypes.Car, 7, VehicleDirections.Right)
            };

            this.vehicleRotateTransform = new RotateTransform
            {
                CenterX = 25, //TODO magic numbers
                CenterY = 25,
                Angle = 180
            };
        }

        #endregion

        public Canvas CreateRoad(double windowHeight, double windowWidth)
        {
            var road = new Canvas
            {
                Height = windowHeight,
                Width = windowWidth
            };
            var distanceFromTop = LaneOneLocation;

            foreach (var lane in this.Lanes)
            {
                road = this.addLaneToRoad(lane, road, distanceFromTop);

                distanceFromTop -= LaneWidth;
            }

            return road;
        }

        private Canvas addLaneToRoad(LaneManager lane, Canvas canvas, int distanceFromTop)
        {
            for (var i = 0; i < lane.Vehicles.Count; i++)
            {
                canvas = this.addVehicleToLane(lane, i, distanceFromTop, canvas);
            }

            return canvas;
        }

        private Canvas addVehicleToLane(LaneManager currentLane, int vehicleIndex, int distanceFromTop, Canvas canvas)
        {
            var vehicle = currentLane.Vehicles[vehicleIndex];
            if (currentLane.Direction == VehicleDirections.Right)
            {
                vehicle.Sprite.RenderTransform = this.vehicleRotateTransform;
            }
            canvas.Children.Add(vehicle.Sprite);
            Canvas.SetTop(vehicle.Sprite, distanceFromTop);
            Canvas.SetLeft(vehicle.Sprite, 200 * vehicleIndex); //TODO magic number

            return canvas;
        }
    }
}