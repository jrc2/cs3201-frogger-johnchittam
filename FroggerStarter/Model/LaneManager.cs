﻿using System.Collections.Generic;

namespace FroggerStarter.Model
{
    /// <summary>
    ///     Manages an individual lane
    /// </summary>
    public class LaneManager
    {
        #region Properties

        /// <summary>
        ///     The vehicle Speed
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        ///     Gets the vehicle's original speed.
        /// </summary>
        /// <value>
        ///     The vehicle's original speed.
        /// </value>
        public double OriginalSpeed { get; }

        /// <summary>
        ///     The direction of all vehicles in the lane
        /// </summary>
        public VehicleDirections Direction { get; }

        /// <summary>
        ///     Gets the vehicles.
        /// </summary>
        /// <value>
        ///     The vehicles.
        /// </value>
        public IList<Vehicle> Vehicles { get; } = new List<Vehicle>();

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LaneManager" /> class.
        /// </summary>
        /// <param name="numVehicles">The number vehicles.</param>
        /// <param name="vehicleType">Type of vehicles.</param>
        /// <param name="speed">The Speed of all vehicles in lane.</param>
        /// <param name="direction">The Direction of all vehicles in lane.</param>
        public LaneManager(int numVehicles, VehicleTypes vehicleType, double speed, VehicleDirections direction)
        {
            this.OriginalSpeed = speed;
            this.Speed = speed;
            this.Direction = direction;

            for (var i = 0; i < numVehicles; i++)
            {
                this.Vehicles.Add(new Vehicle(vehicleType));
            }
        }

        #endregion
    }
}