﻿using FroggerStarter.View.Sprites;

namespace FroggerStarter.Model
{
    /// <summary>
    ///     The vehicle class
    /// </summary>
    /// <seealso cref="FroggerStarter.Model.GameObject" />
    public class Vehicle : GameObject
    {
        #region Data members

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Vehicle" /> class.
        /// TODO add postcondition
        /// </summary>
        /// <param name="vehicleType">Type of the vehicle.</param>
        /// <param name="speed">The vehicle's speed.</param>
        public Vehicle(VehicleTypes vehicleType)
        {
            if (vehicleType == VehicleTypes.Car)
            {
                Sprite = new CarSprite();
            }
            else
            {
                Sprite = new SemiSprite();
            }
        }

        #endregion
    }
}